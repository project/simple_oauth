<?php

namespace Drupal\simple_oauth\Exception;

use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

final class OAuthUnauthorizedHttpException extends UnauthorizedHttpException {}
